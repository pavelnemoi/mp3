<?php

use App\SimplePlayer;
use App\SimpleTrack;

require __DIR__ . '/bootstrap/app.php';

$faker = Faker\Factory::create();
$player = $app->get(SimplePlayer::class);

try {
    for ($i = 0; $i < 10; $i++) {
        $track = new SimpleTrack($faker->uuid, $faker->name);
        $player->add($track);
    }

    $track = new SimpleTrack($faker->uuid, $faker->name);
    $track1 = new SimpleTrack($faker->uuid, $faker->name);

    $player->add([$track,  $track1]);

    echo 'now playing ' . $player->play($track->getId()) . '<br>';

    $next = $player->next();

    echo 'play next... ' . $next->getName() . '<br>';

    echo 'stop playing...<br>';
    $player->stop();

    $prev = $player->prev();
    echo 'play prev... ' . $prev->getName() . '<br>';


    echo 'clear playlist...' . '<br>';
    $player->reset();

    echo 'Playlist count ' . $player->getPlaylist()->getCount();

} catch (InvalidArgumentException $e) {
    echo 'Wrong argument. ' . $e->getMessage();
} catch (LogicException $e) {
    echo 'Error. ' . $e->getMessage();
}