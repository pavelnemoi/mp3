<?php

namespace App\Contracts;

interface Playlist
{
    public function addTrack($id, $track);

    public function removeTrack($id);

    public function getTrack($id);

    public function trackExists($id);

    public function getCount();
}