<?php

namespace App\Contracts;

interface Player
{
    public function add($track);

    public function next();

    public function prev();

    public function stop();

    public function reset();

    public function play($id);

    public function getPlaylist();
}