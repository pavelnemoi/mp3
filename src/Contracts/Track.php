<?php

namespace App\Contracts;

interface Track
{
    public function getId();

    public function getName();
}