<?php

namespace App;

use App\Contracts\Player;
use App\Contracts\Playlist;
use App\Contracts\Track;

class SimplePlayer implements Player
{
    protected $playlist;
    protected $current = null;

    const EMPTY_PLAYLIST    = 'There are no tracks in playlist';
    const INVALID_TRACK     = 'Invalid track passed';
    const NO_TRACK          = 'Track doesn`t exists in current playlist';
    const EMPTY_CURRENT     = 'Current track not specified';

    public function __construct(Playlist $p)
    {
        $this->playlist = $p;
    }

    public function getCurrent()
    {
        return $this->current;
    }

    public function getPlaylist()
    {
        return $this->playlist;
    }

    public function add($track)
    {
        $this->playlist->add($track);

        if (is_array($track)) {
            $this->setCurrent(end($track));
        } else {
            $this->setCurrent($track);
        }

        return $this->playlist->getCount();
    }

    protected function setCurrent(Track $track)
    {
        $this->current = $track;
    }

    public function play($trackId)
    {
        if (!$this->playlist->hasTracks()) {
            throw new \LogicException(self::EMPTY_PLAYLIST);
        }

        if (!$this->playlist->trackExists($trackId)) {
            throw new \InvalidArgumentException(self::NO_TRACK);
        }

        if ($trackId) {
            $this->setCurrent($this->playlist->getTrack($trackId));
        }

        return $this->current->getName();
    }

    public function stop()
    {
        if (!$this->playlist->hasTracks()) {
            throw new \LogicException(self::EMPTY_PLAYLIST);
        }

        if (!$this->hasCurrentTrack()) {
            throw new \LogicException(self::EMPTY_CURRENT);
        }

        return $this->current->getName();
    }

    public function hasCurrentTrack()
    {
        return $this->current->getId() !== false;
    }

    public function reset()
    {
        $this->playlist = new SimplePlaylist();

        return true;
    }

    public function prev()
    {
        if (!$this->playlist->hasTracks()) {
            throw new \LogicException(self::EMPTY_PLAYLIST);
        }

        if ($this->playlist->getCount() == 1) {
            return $this->current;
        }

        // if our current position is first we must choose last track
        $firstTrack = reset($this->playlist);
        if ($this->current->getId() == $firstTrack->getId()) {
            $this->setCurrent(end($this->playlist));
            return end($this->playlist);
        }

        $prev = $this->playlist->getPrev($this->current);
        $this->setCurrent($prev);

        return $this->current;
    }

    public function next()
    {
        if (!$this->playlist->hasTracks()) {
            throw new \LogicException(self::EMPTY_PLAYLIST);
        }

        if ($this->playlist->getCount() == 1) {
            return $this->current;
        }

        // if we stay on the last track and press next system must choose first
        $lastTrack = end($this->playlist);
        if ($lastTrack->getId() == $this->current->getId()) {
            $this->setCurrent(reset($this->playlist));
            return reset($this->playlist);
        }

        $next = $this->playlist->getNext($this->current);

        $this->setCurrent($next);

        return $this->current;
    }
}
