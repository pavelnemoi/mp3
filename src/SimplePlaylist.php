<?php

namespace App;

use App\Contracts\Playlist;

class SimplePlaylist extends \ArrayObject implements Playlist
{
    public function __construct($tracks = [], $flags = 0, $iterator_class = "ArrayIterator")
    {
        parent::__construct($tracks, $flags, $iterator_class);
    }

    public function trackExists($id)
    {
        return $this->offsetExists($id);
    }

    public function getTrack($id)
    {
        return $this->offsetGet($id);
    }

    public function addTrack($id, $track)
    {
        $this->offsetSet($id, $track);
    }

    public function add($track)
    {
        if (is_array($track)) {
            foreach ($track as $item) {
                $this->addTrack($item->getId(), $item);
            }
        } else {
            $this->addTrack($track->getId(), $track);
        }
    }

    public function removeTrack($id)
    {
        $this->offsetUnset($id);
    }

    public function getCount()
    {
        return $this->count();
    }

    public function hasTracks()
    {
        return $this->getCount() != 0;
    }

    public function getCurrent()
    {
        return $this->getIterator()->current();
    }

    public function getPrev($track)
    {
        $it = $this->getIterator();

        while ($it->valid()) {
            $prev = $it->current();
            $it->next();
            $next = $it->current();
            if ($next->getId() == $track->getId()) {
                return $prev;
            }
        }
    }

    public function getNext($track)
    {
        $it = $this->getIterator();

        while ($it->valid()) {
            if ($it->key() == $track->getId()) {
                $it->next();
                return $it->current();
            }
            $it->next();
        }
    }
}