<?php

namespace App;

use App\Contracts\Track;

class SimpleTrack implements Track
{
    protected $id;
    protected $name;

    const INVALID_ID    = 'Invalid id';
    const INVALID_NAME  = 'Invalid name';

    public function __construct($id, $name)
    {
        if (!$id) {
            throw new \InvalidArgumentException(self::INVALID_ID);
        }

        if (!$name || !is_string($name)) {
            throw new \InvalidArgumentException(self::INVALID_NAME);
        }

        $this->id = $id;
        $this->name = $name;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }
}
