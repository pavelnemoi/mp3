####Code Review Report

#####Naming and Structuring

- Mp3 is a container/file format. In your case its better to call interface
'Player' instead.
- Underscores in variables (snake-case) is highly controversial thing. >_<
- As a suggestion move all interfaces to folder `Contacts`.

#####Architecture

- Ideally you can extend `ContainerBuilder` class to make your own app.
- `add()` method should be able to add collection of tracks as well as single
track (See trello).
- Dependency Injection via `PHP-DI` is good enough. But why you don't use it in tests?

- `SimplePlaylist::setCurrent()` visibility is unsafe.

- In `console.php` there are not enough examples. At least `stop()` and `prev()`
are missing.

#####Redundant code

- There are a lot of redundant `use` statements
- `SimpleTrack::setId()` what is this used for?


#####Testing

- How about to generate data with `Faker`?
- Comments in test methods are useless. Make your methods self-explanatory.
- When you test some object specific behavior you **SHOULD NOT** mock it.
You will receive always correct results. E.g:
You're testing `Playlist` and create its mock. This is incorrect cause you're
going to test mock.
It would be much beneficial to mock `Playlist` when you test `Player` because you
don't care about it in this particular scenario.

