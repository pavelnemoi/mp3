<?php

use App\SimpleTrack;

class TrackTest extends PHPUnit_Framework_TestCase
{
    public function testGood()
    {
        $track = new SimpleTrack(1, 'a');
        $this->assertEquals(1, $track->getId());
        $this->assertEquals('a', $track->getName());
    }

    public function testWrongId()
    {
        $this->expectException(InvalidArgumentException::class);
        $track = new SimpleTrack(null, 'a');
    }

    public function testWrongName()
    {
        $this->expectException(InvalidArgumentException::class);
        $track = new SimpleTrack(1, null);
    }
}
