<?php

use App\SimplePlaylist;
use App\SimpleTrack;

class PlaylistTest extends PHPUnit_Framework_TestCase
{
    protected $app;
    protected $playlist;
    const TRACK_COUNT = 5;

    public function setUp()
    {
        parent::setUp();

        $this->app = require __DIR__.'/../bootstrap/app.php';
        $this->playlist = $this->app->get(SimplePlaylist::class);
    }

    public function tearDown()
    {
        parent::tearDown();

        $this->app = null;
    }

    public function generateTracks()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < self::TRACK_COUNT; $i++) {
            $track = new SimpleTrack($faker->uuid, $faker->name);
            $this->playlist->addTrack($track->getId(), $track->getName());
        }
    }

    public function testAdd()
    {
        $this->generateTracks();
        $this->assertEquals($this->playlist->getCount(), self::TRACK_COUNT);
    }

    public function testRemove()
    {
        $this->generateTracks();
        $track = new SimpleTrack(1, 'a');
        $this->playlist->addTrack($track->getId(), $track->getName());
        $this->playlist->removeTrack($track->getId());
        $this->assertEquals($this->playlist->getCount(), self::TRACK_COUNT);
    }

    public function testGetTrack()
    {
        $this->generateTracks();
        $track = new SimpleTrack(1, 'a');

        $this->playlist->addTrack($track->getId(), $track);

        $this->assertEquals($this->playlist->getTrack($track->getId()), $track);
    }
}
