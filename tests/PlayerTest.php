<?php

use App\SimplePlayer;
use App\SimplePlaylist;
use App\SimpleTrack;

class PlayerTest extends PHPUnit_Framework_TestCase
{
    public $player;
    protected $app;

    public function setUp()
    {
        parent::setUp();

        $this->app = require __DIR__. '/../bootstrap/app.php';
        $this->player = $this->app->get(SimplePlayer::class);
    }

    public function tearDown()
    {
        parent::tearDown();

        $this->app = null;
    }

    public function testAdd()
    {
        $track_1 = new SimpleTrack('first', 'Hello');
        $track_2 = new SimpleTrack('second', 'Bye');
        $this->player->add($track_1);
        $this->assertEquals(2, $this->player->add($track_2));
    }

    public function testPlayEmpty()
    {
        $this->expectException(LogicException::class);
        $this->player->play(1);
    }

    public function testPlayWrongTrackId()
    {
        $this->expectException(InvalidArgumentException::class);
        $track_1 = new SimpleTrack('first', 'Hello');
        $this->player->add($track_1);
        $this->player->play(1);
    }

    public function testPlay()
    {
        $track_1 = new SimpleTrack('first', 'Hello');
        $this->player->add($track_1);
        $this->assertEquals($track_1->getName(), $this->player->play($track_1->getId()));
    }

    public function testStopWithEmptyPlaylist()
    {
        $this->expectException(LogicException::class);
        $this->player->stop();
    }

    public function testStop()
    {
        $track_1 = new SimpleTrack('first', 'Hello');
        $this->player->add($track_1);
        $this->player->play($track_1->getId());
        $this->assertEquals($track_1->getName(), $this->player->stop());
    }

    public function testReset()
    {
        $track_1 = new SimpleTrack('first', 'Hello');
        $track_2 = new SimpleTrack('second', 'Bye');

        $this->player->add($track_1);
        $this->player->add($track_2);
        $this->player->reset();

        $this->assertEquals(0, $this->player->getPlayList()->getCount());
    }

    public function testPrev()
    {
        $track_1 = new SimpleTrack('first', 'Hello');
        $track_2 = new SimpleTrack('second', 'Bye');
        $track_3 = new SimpleTrack('third', '333');

        $this->player->add($track_1);
        $this->player->add($track_2);
        $this->player->add($track_3);
        $this->player->play($track_2->getId());

        $this->assertEquals($track_1, $this->player->prev());
    }

    public function testPrevException()
    {
        $this->expectException(LogicException::class);
        $this->player->prev();
    }

    public function testPrevIfCurrentIsFirst()
    {
        $track_1 = new SimpleTrack('first', 'Hello');
        $track_2 = new SimpleTrack('second', 'Bye');

        $this->player->add($track_1);
        $this->player->add($track_2);
        $this->player->play($track_1->getId());

        $this->assertEquals($track_2, $this->player->prev());
    }

    public function testNext()
    {
        $track_1 = new SimpleTrack('1', 'Hello');
        $track_2 = new SimpleTrack('2', 'q');
        $track_3 = new SimpleTrack('3', 'w');
        $track_4 = new SimpleTrack('4', 'e');
        $track_5 = new SimpleTrack('5', 'r');

        $this->player->add($track_1);
        $this->player->add($track_2);
        $this->player->add($track_3);
        $this->player->add($track_4);
        $this->player->add($track_5);

        $this->player->play($track_3->getId());

        $this->assertEquals($track_4->getId(), $this->player->next()->getId());
    }

    public function testNextException()
    {
        $this->expectException(LogicException::class);
        $this->player->next();
    }

    public function testNextIfCurrentIsLast()
    {
        $track_1 = new SimpleTrack('1', 'Hello');
        $track_2 = new SimpleTrack('2', 'q');
        $this->player->add($track_1);
        $this->player->add($track_2);

        $this->player->play($track_2->getId());

        $this->assertEquals($track_1, $this->player->next());
    }
}
