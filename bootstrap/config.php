<?php

use function DI\object;
use App\Contracts\Player;
use App\SimplePlayer;
use App\Contracts\Playlist;
use App\SimplePlaylist;

return [
    // Bind an interface to an implementation
    Player::class      => object(SimplePlayer::class),
    Playlist::class => object(SimplePlaylist::class),
];